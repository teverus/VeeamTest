import hashlib
import os
import sys

ENCRYPTION_TYPES = {
    "md5": hashlib.md5,
    "sha1": hashlib.sha1,
    "sha256": hashlib.sha256
}


class Status:
    OK = "OK"
    FAILURE = "FAIL"
    NOT_FOUND = "NOT FOUND"


def read_file(file_path: str) -> list:
    """
    Returns the contents of a file withing trailing whitespace characters.
    Raises an exception was the file was not found.
    """
    try:
        with open(file_path, "r") as f:
            lines = f.readlines()
        return [line.replace("\n", "") for line in lines]
    except FileNotFoundError:
        raise Exception(f"\n[ERROR] Can't find {file_path}")


def get_file_contents_hash(directory_name: str, _file_name: str, _encryption_type: str) -> str or False:
    """
    Encrypts the contents of the file and returns the hash.
    Raises an exception if directory doesn't exist.
    Raises an exception if the file was not found.
    """
    if not os.path.exists(directory_name):
        raise Exception(f"\n[ERROR] This directory does not exist: {os.path.join(os.getcwd(), directory_name)}")

    file_path = os.path.join(directory_name, _file_name)

    try:
        with open(file_path, "rb") as f:
            file_contents_encryption = ENCRYPTION_TYPES[_encryption_type](f.read()).hexdigest()

        return file_contents_encryption
    except FileNotFoundError:
        print(f"{_file_name} {Status.NOT_FOUND}")
        return False


def check_file_hash(_file_name: str, actual_hash: str, expected_hash: str):
    """
    Compares the hash of the encrypted file and the hash in the config file.
    Prints outs the corresponding status.

    """
    if actual_hash == expected_hash:
        print(f"{_file_name} {Status.OK}")
    else:
        print(f"{_file_name} {Status.FAILURE}")


if __name__ == '__main__':
    if len(sys.argv) != 3:
        raise Exception(f"\n[ERROR] Not enough! arguments\n"
                        f"Please use python <program name> <path to the input file> "
                        f"<path to the directory containing the files to check>")

    input_file_path = sys.argv[1]
    target_directory = sys.argv[2]

    for file in read_file(input_file_path):
        file_name, encryption_type, file_hash = file.split(" ")

        file_contents_hash = get_file_contents_hash(target_directory, file_name, encryption_type)

        if file_contents_hash:
            check_file_hash(file_name, file_contents_hash, file_hash)
