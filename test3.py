import os
import subprocess
import time
from os import listdir
from os.path import expanduser, isfile, join

FILE_NAME = "test"
FILE_SIZE = 1024 * 1000  # 1024 kilobytes


class TestCase:
    def __init__(self, tc_id: int, name: str):
        super(TestCase, self).__init__()
        self.tc_id = tc_id
        self.name = name

    def prep(self):
        pass

    def run(self):
        pass

    def clean_up(self):
        pass

    def execute(self):
        if self.prep():
            self.run()
            self.clean_up()


class ShowHomeDirectoryFilesTestCase(TestCase):
    def __init__(self, tc_id=1, name="Show home directory files"):
        super(ShowHomeDirectoryFilesTestCase, self).__init__(tc_id, name)

    def prep(self) -> bool:
        if int(time.time()) % 2 != 0:
            return False

        return True

    def run(self):
        home_directory = expanduser("~")
        print(f"== Files in home directory ({home_directory}) ==")
        for a_file in [f for f in listdir(home_directory) if isfile(join(home_directory, f))]:
            print(a_file)


class CreateAFileWithRandomContentsTestCase(TestCase):
    def __init__(self, tc_id=2, name="Create a file with random contents"):
        super(CreateAFileWithRandomContentsTestCase, self).__init__(tc_id, name)

    def prep(self) -> bool:
        memory_size = 0
        for element in str(subprocess.check_output("wmic MemoryChip get capacity", shell=True)).split(r"\r\r\n"):
            try:
                memory_size += int(element)
            except ValueError:
                pass

        if memory_size < 1_000_000_000:
            return False

        return True

    def run(self):
        with open(FILE_NAME, "wb") as f:
            f.truncate(1024 * 1000)

        assert os.path.exists(FILE_NAME), "\n[ERROR] The test file was not created."

        assert os.path.getsize(FILE_NAME) == FILE_SIZE, \
            f'\n[ERROR] Expected size was {FILE_SIZE}, but actual size was {os.path.getsize(FILE_NAME)}.'

    def clean_up(self):
        os.remove(FILE_NAME)

        assert not os.path.exists(FILE_NAME), "\n[ERROR] The test file was not removed."


if __name__ == '__main__':
    ShowHomeDirectoryFilesTestCase().execute()
    CreateAFileWithRandomContentsTestCase().execute()
