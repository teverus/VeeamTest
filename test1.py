import os
import xml.etree.cElementTree as ET
from shutil import copyfile
from xml.etree.ElementTree import Element

CONFIG_FILE_NAME = "config.xml"


class Attributes:
    SOURCE_PATH = "source_path"
    DESTINATION_PATH = "destination_path"
    FILE_NAME = "file_name"


class Status:
    SUCCESS = "[SUCCESS]"
    FAILURE = "[FAILURE]"


def get_files_to_copy_from_config_file() -> Element:
    """
    Returns the contents of the config XML file.
    Raises an exception if the file was not found.
    """
    try:
        return ET.parse(CONFIG_FILE_NAME).getroot()
    except FileNotFoundError:
        raise Exception(f'There is no file {CONFIG_FILE_NAME} in {os.getcwd()}')


def get_attribute_from_config_file(file: Element, _index: int, target_attribute: str) -> str:
    """
    Returns a specific attribute from <file> tag.
    Raises an exception if the attribute was not found.
    """
    try:
        return file.attrib[target_attribute]
    except KeyError:
        raise Exception(f'There is no attribute "{target_attribute}" in file tag #{_index + 1}')


def copy_a_file(_file_name: str, _source_path: str, _destination_path: str):
    """
    Copies the specified file from source path to destination path.
    Raises an exception if permission was denied.
    Raises an exception if the source file was not found.
    """
    try:
        print(f"Copying {_file_name} from {_source_path} to {_destination_path}", end=" ")
        copyfile(os.path.join(_source_path, _file_name), os.path.join(_destination_path, _file_name))
        print(Status.SUCCESS)
    except PermissionError:
        print(f"{Status.FAILURE}\n>>> Can't copy {_file_name} to {_destination_path} because permission is denied.")
    except FileNotFoundError:
        print(f"{Status.FAILURE}\n>>> Can't find {_file_name} in {_source_path}.")


if __name__ == '__main__':

    for index, file_to_copy in enumerate(get_files_to_copy_from_config_file()):

        source_path = get_attribute_from_config_file(file_to_copy, index, Attributes.SOURCE_PATH)
        destination_path = get_attribute_from_config_file(file_to_copy, index, Attributes.DESTINATION_PATH)
        file_name = get_attribute_from_config_file(file_to_copy, index, Attributes.FILE_NAME)

        copy_a_file(file_name, source_path, destination_path)
